package net.lctafrica.edge

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver
import org.springframework.cloud.gateway.filter.ratelimit.RedisRateLimiter
import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.annotation.Bean
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.graphql.data.method.annotation.SchemaMapping
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.security.config.Customizer
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.stereotype.Component
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.util.retry.Retry
import java.net.URI
import java.security.Principal
import java.time.Duration

@SpringBootApplication
class EdgeApplication

fun main(args: Array<String>) {
    runApplication<EdgeApplication>(*args)
}

@Bean
fun redisRateLimiter(): RedisRateLimiter {
    return RedisRateLimiter(5, 7)
}

@Bean
fun keyResolver(): KeyResolver {
    return KeyResolver() {
        it.getPrincipal<Principal?>()
            .map<String?>(Principal::getName)
            .switchIfEmpty(Mono.empty())
    }
}

@Bean
fun authentication(): MapReactiveUserDetailsService {

    val user = User.withDefaultPasswordEncoder()
        .apply {
            username("kip").password("password").roles("USER")
        }.build()
    return MapReactiveUserDetailsService(user)

}

@Bean
fun authorization(security: ServerHttpSecurity): SecurityWebFilterChain {

    val customizer = Customizer.withDefaults<ServerHttpSecurity.HttpBasicSpec>()

    return security.authorizeExchange() { ae ->
        ae.pathMatchers("/proxy")
            .authenticated()
            .anyExchange().permitAll()
    }.httpBasic() {
        customizer
    }.csrf() {
        it.disable()
    }.build()
}

@Bean
fun gateway(
    rlb: RouteLocatorBuilder,
    redisRateLimiter: RedisRateLimiter,
    keyResolver: KeyResolver
): RouteLocator {

    return rlb.routes()
        .route { a ->
            a.path("/proxy")
                .and()
                .host("*.spring.io")
                .filters { b ->
                    b.setPath("/customers")
                        .addRequestHeader("Access-Control-Allow-Origin", "*")
                        .requestRateLimiter { rl ->
                            rl.rateLimiter = redisRateLimiter
                            rl.keyResolver = keyResolver
                        }
                }
                .uri("http://localhost:8080/")
        }
        .build()

/*
   return rlb.routes()
       .route(){a->
           a.path("/api/v1/membership","/api/v1/provider","/api/v1/country","/api/v1/catalog")
               .uri("http://188.166.206.253:8070")
       }
       .route(){ b ->
           b.path("/get")
               .uri("http://httpbin.org:80")
       }
       .build()
*/
}

@Bean
fun webClient(): WebClient {
    return WebClient.builder()
        .baseUrl("abc")
        .build()
}

@Bean
fun rSocketRequester(): RSocketRequester {
    return RSocketRequester.builder()
        .tcp("localhost", 8181)
}

@Component
class CrmClient {
    val http: WebClient = webClient()
    val rSocket: RSocketRequester = rSocketRequester()

    fun getCustomers(): Flux<Customer> {
        return http.get().uri("http://localhost:8080/customers")
            .retrieve().bodyToFlux(Customer::class.java)
            .onErrorResume { Flux.empty<Customer>() }
            .retryWhen(Retry.backoff(10, Duration.ofSeconds(1)))
            .timeout(Duration.ofSeconds(1))
    }

    fun getOrdersFor(customerId: Int): Flux<Order> {
        return rSocket.route("orders.{cid}", customerId)
            .retrieveFlux(Order::class.java)
            .onErrorResume { Flux.empty<Order>() }
            .retryWhen(Retry.backoff(10, Duration.ofSeconds(1)))
            .timeout(Duration.ofSeconds(1))
    }

    fun getCustomerOrders(): Flux<CustomerOrders> {
        return getCustomers()
            .flatMap<CustomerOrders?>
            { customer ->
                Mono.zip(
                    Mono.just(customer),
                    getOrdersFor(customer.id).collectList()
                ).map { tuple2 -> CustomerOrders(tuple2.t1, tuple2.t2) }
            }
    }

}

data class CustomerOrders(val customer: Customer, val orders: List<Order>)
data class Customer(val id: Int, val name: String)
data class Order(val id: Int, val customerId: Int)

@Controller
@ResponseBody
class CrmRestController(val crmClient: CrmClient) {
    @GetMapping("/cos")
    fun get(): Flux<CustomerOrders> {
        return crmClient.getCustomerOrders()
    }
}

@Controller
class CrmGraphqlController(val crmClient: CrmClient) {

    @SchemaMapping(typeName = "Customer")
    fun orders(customer: Customer): Flux<Order> {
        return crmClient.getOrdersFor(customer.id)
    }

    @QueryMapping
    fun customers(): Flux<Customer> {
        return crmClient.getCustomers()
    }

}